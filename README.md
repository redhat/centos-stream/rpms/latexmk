[Latexmk](http://personal.psu.edu/jcc8/software/latexmk-jcc/) is a perl script
for running LaTeX the correct number of times to resolve cross references,
etc.; it also runs auxiliary programs (bibtex, makeindex if necessary, and
dvips and/or a previewer as requested).  It has a number of other useful
capabilities, for example to start a previewer and then run latex whenever the
source files are updated, so that the previewer gives an up-to-date view of
the document.  The script runs on both UNIX and MS-WINDOWS (95, ME, XP, etc).
This script is a corrected and improved version of the original version of
latexmk.
